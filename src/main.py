import re

from dotenv import load_dotenv
load_dotenv()

from flask import Flask
from services import GitLab
from services import OpenAI
from flask import jsonify

app = Flask(__name__)

@app.route('/<projectId>/<jobId>')
def getFix(projectId, jobId):
    GitLabJobTrace = GitLab.GetTrace(projectId, jobId)
    #print(GitLabJobTrace)
    pattern = re.compile(r"(Module not found|failed to compile)(.*?)$", re.IGNORECASE | re.MULTILINE | re.DOTALL)
    matches = re.findall(pattern, str(GitLabJobTrace))

    error_lines = [match[1].strip() for match in matches]


    print(error_lines)
    OPENAI_PROMPT = f'''
        GitLab CICD pipeline job log shows the following error in the pipeline.
        {error_lines}
        Can you give the solution to fix it? I only need the command as the output. No need any additinal commands or descriptions.
        Dont send the 'Answer:' Phrase in the output. Just the command I want 
    '''
    print(OPENAI_PROMPT)
    res = OpenAI.getRes(OPENAI_PROMPT)
    print(res)

    return res

app.run(host='0.0.0.0', port=8080)
