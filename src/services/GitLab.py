import os
from services import API

GITLAB_PRIVATE_TOKEN = os.getenv("GITLAB_PRIVATE_TOKEN")


def GetTrace(projectId, jobId):
    return API.GET(
        f"https://gitlab.com/api/v4/projects/{projectId}/jobs/{jobId}/trace",
        {'PRIVATE-TOKEN': GITLAB_PRIVATE_TOKEN},
        False
    )
