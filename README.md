## Getting Started

1. Install Python 3:

      Go to [install Python 3](https://www.python.org/downloads/)

2. Install Required Python Packages:

    ```
    $ pip install -r requirements.txt
    ```

3. Update Env Variables

    ```
    $ cp .env.sample .env
    $ then update the .env file
    ```

<hr/>

## Usage
  
  ```
  $ python src/main.py
  ```

  ```
  $ curl http://127.0.0.1:8080/<projectId>/<jobId>
  eg: curl http://127.0.0.1:8080/34/8104
  ```